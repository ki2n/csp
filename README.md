README #

Syntax: `csp -e | -x [message] carrier output`
Options:
	-e	Embed mode
	-x	Extract mode
	message Embed mode only, the file to be embedded
	carrier	The image file to carry the file
	output	The resulting image (embed mode) or message (extract mode) 

### Summary and Changelog ###

csp is a basic command-line encryption and steganography tool. It employs Camellia-256
encryption for message security.

Changelog
* Future Releases
 * 0.0.2	Add additional options to separate encryption and embedding
		and also add preliminary multithreading encryption.
* Current Releases
 * 0.0.1	Initial release


### Setup, Installation ###

Once you download this application, just place it into your system directory, preferably within /usr/bin. Then it's ready to go!

**Dependencies:** 
		`steganography python-camellia hashlib` 

**Platform-dependent Dependencies:**
		`getpass base64 [multithreading]*`
		*will be a dependency in 0.0.2

### Contribution guidelines ###

If you want to contribute, please feel free to contact me.

### Contact ###

You can contact me at kettuttek@gmail.com.
