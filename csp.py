#!/usr/bin/python

"""
    CSP v 0.0.1
    by ki2ne

    This application embeds a message or some data to an image.
    Usage: csp -e | -x [message] carrier output
    Passwords will be always prompted.

    -e       Enables embedding mode.
    -x       Enables extraction mode.
    message  (Embedding Mode) Message or data to embed.
    carrier  The image to embed the message to.
    output   Output image/message.
"""

import os, sys, hashlib, camellia, base64, getpass 
from steganography.steganography import Steganography as stego

# Multiply by 8 to find the bitlength.
KEY_BYTELEN = 32; SALT_BYTELEN = 32
IV_BYTELEN = 16; BLOCK_BYTESIZE = 16


# This will return a tuple of cipherdata, salt, and IV.
def cspEncrypt(data, password, salt = None, iv = None):

    if salt == None:
        cspSalt = os.urandom(SALT_BYTELEN)
    else:
        cspSalt = salt

    if iv == None:
        cspIV = os.urandom(IV_BYTELEN)
    else:
        cspIV = iv

    cspPadLen = 0

    # Key derivation using HMAC-SHA512 and about 58,000 iterations
    cspKey = hashlib.pbkdf2_hmac('sha512', password, cspSalt, 582013, KEY_BYTELEN)

    cspCipher = camellia.CamelliaCipher(key = cspKey, IV = cspIV, mode = camellia.MODE_CBC)
    if len(data) % BLOCK_BYTESIZE > 0:
        cspPadLen = BLOCK_BYTESIZE - (len(data) % BLOCK_BYTESIZE)
        data += "\0" * cspPadLen 

    return (cspCipher.encrypt(data), cspSalt, cspIV, cspPadLen)

def cspDecrypt(data, password, salt, iv, padsize = 0):
    if len(data) % BLOCK_BYTESIZE > 0:
        raise ValueError("Data is not in %d-bit blocks." % (BLOCK_BYTESIZE * 8))
    if len(salt) < SALT_BYTELEN:
        raise ValueError("Salt is not in the correct size.") 
    
    cspKey = hashlib.pbkdf2_hmac('sha512', password, salt, 582013, KEY_BYTELEN)
    cspCipher = camellia.CamelliaCipher(key = cspKey, IV = iv, mode = camellia.MODE_CBC)
    if padsize > 0:
        return cspCipher.decrypt(data)[:-padsize]
    else:
        return cspCipher.decrypt(data)


# Data format: Salt + IV + (Pad Length+97) + Cipher 
def cspEmbed(source, destination, data, password, salt = None, iv = None):
    ivsalt = None
    if salt != None and iv != None:
        ivsalt = salt; ivsalt += iv
    elif salt != None and iv == None:
        ivsalt = salt; ivsalt += os.urandom(IV_BYTELEN)
    elif salt == None and iv != None:
        ivsalt = os.urandom(SALT_BYTELEN); ivsalt += iv
    else:
        ivsalt = os.urandom(SALT_BYTELEN + IV_BYTELEN)
    
    cspCode = cspEncrypt(data, password, ivsalt[:32], ivsalt[32:])
    if cspCode[3] > 0:
        outCode = ivsalt + chr(cspCode[3] + 97) + cspCode[0]
    else:
        outCode = ivsalt + chr(97) + cspCode[0]
    stego.encode(source, destination, base64.b64encode(outCode))

def cspExtract(source, password):
    cspRawCode = base64.b64decode(stego.decode(source))
    salt = cspRawCode[:32]; iv = cspRawCode[32:48]
    padding = ord(cspRawCode[48]) - 97
  
    cspCode = cspRawCode[49:]
    return cspDecrypt(cspCode, password, salt, iv, padding) 

# Interactive password prompt mode
prompt_password = getpass.getpass()
argc = len(sys.argv)
if argc > 1:
     if sys.argv[1] == "-x":
         data = cspExtract(sys.argv[2], prompt_password); f = open(sys.argv[3], 'wb')
         f.write(data); f.close()

     elif sys.argv[1] == "-e":
         f = open(sys.argv[2], 'rb')
         cspEmbed(sys.argv[3], sys.argv[4], f.read(), prompt_password)
         f.close()

     else:
         print "%s -e | -x [message] carrier output" % sys.argv[0]
else:
     print "%s -e | -x [message] carrier output" % sys.argv[0]
